# Saiga Challenge

A test application created with flutter.

Possible enhancements:

- Exception error dialogs can be displayed.

- Add unit testing.

- Store user or session in localstorage or similar.

- Logout functionality.


Features:

- Login with user from https://jsonplaceholder.typicode.com/users

- Visualize todo data from https://jsonplaceholder.typicode.com/todos

- Create todos.

- Delete todos using swipe gesture.


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
