import 'package:flutter/material.dart';
import 'package:saiga/models/todo.dart';
import 'package:google_fonts/google_fonts.dart';

typedef ChangeTodoCallback = void Function(Todo todo, bool value);
typedef DeleteTodoCallback = void Function(Todo todo);

class TodosList extends StatelessWidget {
  final List<Todo> todos;
  final ChangeTodoCallback changeTodo;
  final DeleteTodoCallback deleteTodo;

  TodosList(
      {@required this.todos,
      @required this.changeTodo,
      @required this.deleteTodo});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: todos.map((todo) {
        return Dismissible(
            key: ValueKey(todo.id),
            background: Container(
              color: Colors.red,
              child: Icon(Icons.delete, color: Colors.white, size: 30),
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 20),
            ),
            direction: DismissDirection.endToStart,
            confirmDismiss: (direction) {
              return showDialog(
                  context: context,
                  builder: (ctx) => AlertDialog(
                          title: Text('Delete Todo'),
                          content: Text(
                              'Are you sure you want to delete this todo?'),
                          actions: <Widget>[
                            TextButton(
                              child: Text('No'),
                              onPressed: () {
                                Navigator.of(ctx).pop(false);
                              },
                            ),
                            TextButton(
                              child: Text('Yes'),
                              onPressed: () {
                                Navigator.of(ctx).pop(true);
                              },
                            ),
                          ]));
            },
            onDismissed: (driection) {
              deleteTodo(todo);
            },
            child: Card(
                child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                      child: Checkbox(
                        checkColor: Colors.white,
                        activeColor: Color(0xFF4a97f2),
                        value: todo.completed,
                        onChanged: (bool value) {
                          changeTodo(todo, value);
                          // setState(() {
                          //   todo.completed = value;
                          // });
                        },
                      )),
                ),
                Expanded(
                    flex: 8,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                      child: Text(todo.title,
                          style: GoogleFonts.roboto(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              decoration: todo.completed
                                  ? TextDecoration.lineThrough
                                  : TextDecoration.none)),
                    )),
              ],
            )));
      }).toList(),
    );
  }
}
