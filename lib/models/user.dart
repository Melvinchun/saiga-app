import 'package:flutter/cupertino.dart';
import 'package:saiga/models/Company.dart';
import 'package:saiga/models/address.dart';

class User {
  int id;
  String name;
  String username;
  String email;
  String phone;
  String website;
  Address address;
  Company company;

  User({
    @required this.id,
    @required this.name,
    @required this.username,
    @required this.email,
    this.phone,
    this.website,
    this.address,
    this.company,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    Address address = Address.fromJson(json['address']);
    Company company = Company.fromJson(json['company']);
    return User(
        id: json['userId'],
        name: json['name'],
        username: json['username'],
        email: json['email'],
        phone: json['phone'],
        website: json['website'],
        address: address,
        company: company);
  }
}
