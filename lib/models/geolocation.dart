import 'package:flutter/cupertino.dart';

class Geolocation {
  String lat;
  String lng;

  Geolocation({
    @required this.lat,
    @required this.lng,
  });

  factory Geolocation.fromJson(Map<String, dynamic> json) {
    return Geolocation(
      lat: json['lat'],
      lng: json['lng'],
    );
  }
}
