import 'package:flutter/cupertino.dart';

class Todo {
  int userId;
  int id;
  String title;
  bool completed;

  Todo({@required this.title, this.id, this.userId, this.completed = false});

  factory Todo.fromJson(Map<String, dynamic> json) {
    return Todo(
      title: json['title'],
      id: json['id'],
      userId: json['userId'],
      completed: json['completed'],
    );
  }
}
