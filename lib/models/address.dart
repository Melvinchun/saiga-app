import 'package:flutter/cupertino.dart';
import 'package:saiga/models/geolocation.dart';

class Address {
  String street;
  String suite;
  String city;
  String zipcode;
  Geolocation geolocation;

  Address(
      {@required this.street,
      @required this.city,
      @required this.zipcode,
      this.suite,
      this.geolocation});

  factory Address.fromJson(Map<String, dynamic> json) {
    Geolocation geolocation = Geolocation.fromJson(json['geo']);
    return Address(
        street: json['street'],
        city: json['city'],
        zipcode: json['zipcode'],
        suite: json['suite'],
        geolocation: geolocation);
  }
}
