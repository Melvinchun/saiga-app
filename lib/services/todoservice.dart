import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:saiga/models/todo.dart';

final String baseURL = 'jsonplaceholder.typicode.com';

class TodoService {
  Future<List<Todo>> fetchTodos() async {
    final response = await http.get(Uri.https(baseURL, '/todos'));
    if (response.statusCode == 200) {
      List responseJson = jsonDecode(response.body);
      return responseJson.map((user) => Todo.fromJson(user)).toList();
    } else {
      throw Exception('Failed to load todos');
    }
  }
}
