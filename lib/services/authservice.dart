import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:saiga/pages/login_page.dart';
import 'package:saiga/models/user.dart';

final String baseURL = 'jsonplaceholder.typicode.com';

class AuthService {
  //Determine if the user is authenticated.
  handleAuth() {
    return LoginPage();
  }

  Future<User> login({String email, String password}) async {
    try {
      var queryParameters = {
        'email': email,
      };
      final response =
          await http.get(Uri.https(baseURL, '/users', queryParameters));
      if (response.statusCode == 200) {
        List responseJson = jsonDecode(response.body) as List;
        if (responseJson.length == 1) {
          return User.fromJson(responseJson[0]);
        } else {
          throw Exception('User not found');
        }
      } else {
        throw Exception('Failed to load user');
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
