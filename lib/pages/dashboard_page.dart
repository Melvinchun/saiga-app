import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:saiga/services/todoservice.dart';
import 'package:saiga/models/todo.dart';
import 'package:saiga/widgets/todos_list.dart';

class DashboardPage extends StatefulWidget {
  DashboardPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  final _formKey = GlobalKey<FormState>();
  int _mockID = 1000;
  bool _isLoading = true;
  String _title = '';
  List<Todo> todos = [];

  @override
  void initState() {
    super.initState();
    refresh();
  }

  void refresh() async {
    List<Todo> temp = await TodoService().fetchTodos();
    setState(() {
      todos = temp;
      _isLoading = false;
    });
  }

  void changeTodo(Todo todo, bool value) {
    setState(() {
      todo.completed = value;
    });
  }

  void deleteTodo(Todo todo) {
    setState(() {
      todos.remove(todo);
    });
  }

  void addTodo(ctx) {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      Todo newTodo = Todo(title: _title, id: _mockID++);
      setState(() {
        todos.insert(0, newTodo);
        _title = '';
      });
      Navigator.of(ctx).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              Text('TODOS',
                  style: GoogleFonts.roboto(
                    color: Color(0xFFffba34),
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                  )),
              Text('List',
                  style: GoogleFonts.roboto(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w300,
                  )),
              Text('.',
                  style: GoogleFonts.roboto(
                    color: Color(0xFFffba34),
                    fontSize: 20,
                    fontWeight: FontWeight.w800,
                  ))
            ],
          ),
          elevation: 0,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
        ),
        body: Center(
            child: _isLoading
                ? SpinKitSquareCircle(
                    color: Color(0xFF4a97f2),
                    size: 20.0,
                  )
                : TodosList(
                    todos: todos,
                    changeTodo: changeTodo,
                    deleteTodo: deleteTodo)),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xFF4a97f2),
          onPressed: () {
            showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                        title: Text('Add Todo'),
                        content: Form(
                            key: _formKey,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: 'Title',
                                  labelStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  )),
                              onChanged: (value) {
                                _title = value;
                              },
                              validator: (value) =>
                                  value.isEmpty ? 'Title is required' : null,
                            )),
                        actions: <Widget>[
                          TextButton(
                            child: Text('Cancel'),
                            onPressed: () {
                              Navigator.of(ctx).pop();
                            },
                          ),
                          TextButton(
                            child: Text('Add'),
                            onPressed: () {
                              addTodo(ctx);
                            },
                          ),
                        ]));
          },
          tooltip: 'Add Todo',
          child: Icon(Icons.add),
        ));
  }
}
