import 'package:flutter/material.dart';
import 'package:saiga/models/user.dart';
import 'package:saiga/services/authservice.dart';
import 'package:saiga/pages/dashboard_page.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = new GlobalKey<FormState>();
  String _email;
  String _password;
  bool _loading = false;
  bool _showPassword = false;

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return !regex.hasMatch(value) ? 'Enter Valid Email' : null;
  }

  void onLogin() async {
    setState(() {
      _loading = true;
    });
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();

      User user = await AuthService().login(email: _email, password: _password);

      if (user != null) {
        setState(() {
          _email = '';
          _password = '';
        });
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => DashboardPage()));
      }
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: _formKey,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: Image.asset(
                'assets/img/logo.png',
                width: 200,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: TextFormField(
                decoration: InputDecoration(
                  icon: Icon(Icons.verified_user),
                  labelText: 'Email',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onChanged: (value) {
                  _email = value;
                },
                validator: (value) =>
                    value.isEmpty ? 'Email is required' : validateEmail(value),
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: TextFormField(
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock),
                      labelText: 'Password',
                      labelStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                      suffixIcon: InkWell(
                          onTap: () {
                            setState(() {
                              _showPassword = !_showPassword;
                            });
                          },
                          child: Icon(_showPassword
                              ? Icons.visibility_off
                              : Icons.visibility))),
                  obscureText: !_showPassword,
                  onChanged: (value) {
                    _password = value;
                  },
                  validator: (value) =>
                      value.isEmpty ? 'Password is required' : null,
                )),
            SizedBox(
              height: 30.0,
            ),
            GestureDetector(
                onTap: () {
                  onLogin();
                },
                child: Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    height: 40.0,
                    child: Material(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Color(0xFF4a97f2),
                        child: GestureDetector(
                            child: Center(
                                child: _loading
                                    ? SpinKitSquareCircle(
                                        color: Colors.white,
                                        size: 20.0,
                                      )
                                    : Text('Login',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ))))))),
          ],
        ),
      ),
    ));
  }
}
